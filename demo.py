#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import os
from timeit import time
import warnings
import sys
import cv2
import numpy as np
from PIL import Image
from yolo import YOLO

from deep_sort import preprocessing
from deep_sort import nn_matching
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker
from tools import generate_detections as gdet
import csv
import argparse
warnings.filterwarnings('ignore')

#deep sort constants
MAX_COSINE_DISTANCE = 0.4
NMS_MAX_OVERLAP = 0.5
MAX_IOU_DISTANCE = 0.5
MODEL_FILENAME = 'model_data/mars-small128.pb'
NN_BUDGET = None
MAX_AGE = 5
N_INIT = 3

#YOLO constants
MOTOR_VEHICLE_CLASSES = ['car', 'motorbike', 'bus', 'truck']

#output video constants
TRACKING_COLOR = (0, 255, 0)
DETECTION_COLOR = (255, 0, 0)
TEXT_SCALE = 0.5
TEXT_THICKNESS = 2
BBOX_THICKNESS = 2


def main(yolo, input_video_path, output_video_path, output_log_path):
    #deep sort
    encoder = gdet.create_box_encoder(MODEL_FILENAME,batch_size=1)
    metric = nn_matching.NearestNeighborDistanceMetric("cosine", MAX_COSINE_DISTANCE, NN_BUDGET)
    tracker = Tracker(metric, max_age=MAX_AGE, n_init=N_INIT, max_iou_distance=MAX_IOU_DISTANCE)

    video_capture = cv2.VideoCapture(input_video_path)

    # Define the codec and create VideoWriter object
    w = int(video_capture.get(3))
    h = int(video_capture.get(4))
    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    out = cv2.VideoWriter(output_video_path, fourcc, 15, (w, h))
    if sys.version_info > (3, 0):
        log_file = open(output_log_path, 'w', newline='')
    else:
        log_file = open(output_log_path, 'wb')
    log_file_writer = csv.writer(log_file, delimiter=',')
    log_file_writer.writerow(['frame', 'object_id', 'min_x', 'min_y', 'max_x', 'max_y'])
    frame_index = 0
        
    fps = 0.0
    while True:
        ret, frame = video_capture.read()  # frame shape 640*480*3
        if not ret:
            break
        t1 = time.time()

        image = Image.fromarray(frame)
        boxs, scores = yolo.detect_image(image)

        features = encoder(frame,boxs)
        detections = [Detection(bbox, score, feature) for bbox, score, feature in zip(boxs, scores, features)]
        
        # Run non-maxima suppression.
        boxes = np.array([d.tlwh for d in detections])
        indices = preprocessing.non_max_suppression(boxes, NMS_MAX_OVERLAP, scores)
        detections = [detections[i] for i in indices]
        
        # Call the tracker
        tracker.predict()
        tracker.update(detections)

        (_, txt_h), _ = cv2.getTextSize('', 0, TEXT_SCALE, TEXT_THICKNESS)
        for track in tracker.tracks:
            if not track.is_confirmed():
                continue 
            bbox = track.to_tlbr()
            bbox_int = tuple(bbox.astype(np.int))

            #output to frame
            cv2.rectangle(frame, bbox_int[:2], bbox_int[2:], TRACKING_COLOR, BBOX_THICKNESS)
            cv2.putText(frame, 'ID:{:d}'.format(track.track_id),
                        (bbox_int[0], bbox_int[1] - txt_h // 2), 0, TEXT_SCALE, TRACKING_COLOR, TEXT_THICKNESS)

            #output to log file
            log_file_writer.writerow([frame_index, track.track_id] + bbox.tolist())

        for det in detections:
            bbox = det.to_tlbr()
            bbox_int = tuple(bbox.astype(np.int))

            cv2.rectangle(frame, bbox_int[:2], bbox_int[2:], DETECTION_COLOR, BBOX_THICKNESS)
            cv2.putText(frame, 'P:{:.2f}'.format(det.confidence),
                        (bbox_int[0], int(bbox[1] + det.tlwh[-1] + 1.5 * txt_h)), 0, TEXT_SCALE, DETECTION_COLOR, TEXT_THICKNESS)
            
        cv2.imshow('', frame)
        # save a frame
        out.write(frame)
        frame_index += 1
            
        fps = (fps + (1./(time.time()-t1))) / 2
        print("fps= %f" % fps)
        
        # Press Q to stop!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    video_capture.release()
    out.release()
    log_file.close()
    cv2.destroyAllWindows()


def video_path(path):
    """ Checks if the string is a valid path to a video. """
    _, ext = os.path.splitext(path)
    if ext.lower() not in ['.avi','.mp4'] or not os.path.exists(path):
        raise argparse.ArgumentTypeError('should be an existing AVI or MP4 file')
    return path


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description='Deep sort YOLOv3 car tracker demo.')
    parser.add_argument('input_video_path', type=video_path, help='path to the input video (works with AVI and might work with MP4)')
    parser.add_argument('-o', dest='output_video_path', help='path to output video (AVI)', default='output.avi')
    parser.add_argument('-l', dest='output_log_path', help='path to the output log file', default='output.csv')
    args = parser.parse_args()

    main(YOLO(class_filter=MOTOR_VEHICLE_CLASSES), args.input_video_path, args.output_video_path, args.output_log_path)
