
# Introduction
Real-time multi-car tracker. This is a repurposed Multi-person tracker with some additional functionality. It uses YOLO v3 and deep_sort with tensorflow.
The original project can be found [here](https://github.com/Qidian213/deep_sort_yolov3). Adaptation is not perfect since it still uses 
deep_sort's person re-indentification weights for feature generation. One should train a network on vehicle re-identification dataset for 
better results.

# Quick Start

1. Download YOLOv3-416 weights from [YOLO website](http://pjreddie.com/darknet/yolo/).
2. Convert the Darknet YOLO model to a Keras model.
3. Run YOLO_DEEP_SORT on an arbitrary video.

The following three steps, you can change accordingly:

```
   download the weights at first from yolo website or use your own weights. 
   python convert.py yolov3.cfg yolov3.weights model_data/yolo.h5
   python demo.py example/example.mp4
```

#Demo usage
```
python demo.py [-h] [-o OUTPUT_VIDEO_PATH] [-l OUTPUT_LOG_PATH] input_video_path

```

positional arguments:
 
**input_video_path**: Path to the input video. Works with AVI and might work
                        with MP4.


optional arguments:

**-h, --help**: show help message and exit

**-o**: path to output video (AVI) (default: output.avi)

**-l**: path to the output log file (default: output.csv)

Compared to the original project, the demo script also shows YOLOv3 detection probabilities in the output video and
outputs a more structured csv file with following columns: frame index, tracked object id, x-coordinate of top-left
bbox corner, y-coordinate of top-left bbox corner, x-coordinate of bottom-right bbox corner, y-coordinate of bottom-right
 bbox corner. 
 
 Some errors (e.g.suppressing deep_sort's max_age parameter) that were impacting tracker's robustness in the original project were also fixed. 
 

# Note 
 file model_data/yolo.h5 is to large too upload, so you need convert it from Darknet Yolo model to a keras model by yourself
 
 yolo.h5 model can be downloaded from https://drive.google.com/file/d/1uvXFacPnrSMw6ldWTyLLjGLETlEsUvcE/view?usp=sharing
 
# Example

Example input video file and it's outputs can be found in [example folder](example/).

 

